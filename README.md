# Vending SDK for iOS

[Mastercard Vending]

### Swift
- Download the latest release of [VendingSDK].
- Go to your Xcode project’s “General” settings. Drag VendingSDK.framework to the “Embedded Binaries” section. Make sure Copy items if needed is selected and click Finish.
- Create a new “Run Script Phase” in your app’s target’s “Build Phases” and paste the following snippet in the script text field:
-      bash "${BUILT_PRODUCTS_DIR}/${FRAMEWORKS_FOLDER_PATH}/VendingSDK.framework/strip-frameworks.sh"
    This step is required to work around an App Store submission bug when archiving universal binaries.

### Objc
- Follow same instructions as Swift
- Go to your Xcode project's "Build Settings" and set "Always Embed Swift Standard Libraries" to "YES"

#
   [Vending SDK]: <url>
   [Mastercard Vending]: <https://developer.mastercard.com/product/mastercard-vending>
