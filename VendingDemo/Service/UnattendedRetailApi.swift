//
//  UnattendedRetailApi.swift
//  VendingDemo
//
//  Created by Muhammad Azeem on 9/20/16.
//  Copyright © 2016 Muhammad Azeem. All rights reserved.
//

import Foundation
import Moya
import Alamofire

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data //fallback to original data if it cant be serialized
    }
}

let UnattendedRetailProvider = MoyaProvider<UnattendedRetail>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])

public func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}

enum UnattendedRetail {
    case nearbyMachines(latitude: Float, longitude: Float)
}

extension UnattendedRetail: TargetType {
    public var baseURL: URL { return URL(string: Bundle.main.object(forInfoDictionaryKey: "VendingServerURL") as! String)! }
    public var path: String {
        switch self {
        case .nearbyMachines(_, _):
            return "/machines"
        }
    }
    public var method: Moya.Method {
        return .GET
    }
    public var parameters: [String: Any]? {
        switch self {
        case .nearbyMachines(let latitude, let longitude):
            return ["latitude": latitude, "longitude": longitude]
        }
    }
    public var task: Task {
        return .request
    }
    public var sampleData: Data {
        switch self {
        case .nearbyMachines(_, _):
            return "[{\"name\": \"machine\", \"id\": 100}]".data(using: .utf8)!
        }
    }
}
