//
//  Machine.swift
//  VendingDemo
//
//  Created by Muhammad Azeem on 9/20/16.
//  Copyright © 2016 Muhammad Azeem. All rights reserved.
//

import Foundation
import ObjectMapper

class Machine : Mappable {
    var name: String!
    var distance: Float!
    var identifier: String!
    var model: String!
    var serial: String!
    var serviceId: String!
    var latitude: Float!
    var longitude: Float!
    
    required init?(map: Map) {
        
    }
    
    /// This function is where all variable mappings should occur. It is executed by Mapper during the mapping (serialization and deserialization) process.
    func mapping(map: Map) {
        name <- map["name"]
        distance <- map["distance"]
        identifier <- map["identifier"]
        model <- map["model"]
        serial <- map["serial"]
        serviceId <- map["serviceId"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}
