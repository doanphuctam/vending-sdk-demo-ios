//
//  NavigationController.swift
//  VendingDemo
//
//  Created by Muhammad Azeem on 9/27/16.
//  Copyright © 2016 Muhammad Azeem. All rights reserved.
//

import UIKit

class NavigationController : UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        self.navigationBar.titleTextAttributes = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 28)]
    }
}

extension NavigationController : UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController is ViewController {
            self.navigationBar.setBackgroundImage(nil, for: .default)
            self.navigationBar.barTintColor = UIColor.squash
        } else {
            self.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationBar.shadowImage = UIImage()
        }
    }
}
