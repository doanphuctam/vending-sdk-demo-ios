//
//  ViewController.swift
//  VendingDemo
//
//  Created by Muhammad Azeem on 9/19/16.
//  Copyright © 2016 Muhammad Azeem. All rights reserved.
//

import UIKit
import Moya
import CoreLocation

let CellIdentifier = "Cell"

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl: UIRefreshControl!
    
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation?
    
    var nearbyMachines = [Machine]()
    
    var nearbyMachineRequest: Cancellable?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSLocalizedString("AnyDrink", comment: "")
        
        self.tableView.register(HeaderView.self, forHeaderFooterViewReuseIdentifier: "header")
        
        guard let serverUrl = (Bundle.main.object(forInfoDictionaryKey: "VendingServerURL") as? String), !serverUrl.isEmpty else {
            let vc = UIAlertController(title: "Error", message: "You need to set VendingServerURL in Info.plist for this demo to work properly", preferredStyle: .alert)
            vc.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            
            self.present(vc, animated: true, completion: nil)
            return
        }
        
        self.locationManager = CLLocationManager()
        locationManager.delegate = self
        
        self.refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        
        refresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if "machineDetail" == segue.identifier,
            let vc = segue.destination as? MachineViewController,
            let indexPath = self.tableView.indexPathForSelectedRow {
            
            vc.machine = self.nearbyMachines[indexPath.row]
        }
    }
    
    // MARK:- Private methods
    func refresh() {
        refreshControl.beginRefreshing()
        locationManager.requestLocation()
    }
    
    func fetchNearbyMachines(force: Bool = false) {
        guard let currentLocation = currentLocation else {
            return
        }
        
        if let request = nearbyMachineRequest, request.cancelled {
            if force {
                request.cancel()
            } else {
                return
            }
        }
        
        let latitude = Float(currentLocation.coordinate.latitude)
        let longitude = Float(currentLocation.coordinate.longitude)
        
        nearbyMachineRequest = UnattendedRetailProvider.request(.nearbyMachines(latitude: Float(latitude), longitude: Float(longitude))) { result in
            self.refreshControl.endRefreshing()
            switch result {
            case let .success(response):
                do {
                    print(response)
                    self.nearbyMachines = try response.mapArray(type: Machine.self)
                } catch {
                    self.showAlert(title: "Unattended retail fetch", message: "Unable to fetch from Unattended retail service")
                }
                self.tableView.reloadData()
            case let .failure(error):
                guard let error = error as? CustomStringConvertible else {
                    break
                }
                self.showAlert(title: "Nearby machines", message: error.description)
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        let vc = UIAlertController(title: title, message: message, preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            
        present(vc, animated: true, completion: nil)
    }
}

// MARK: - Table view methods
extension ViewController : UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nearbyMachines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? VendingCell else {
            return UITableViewCell()
        }
        
        let machine = nearbyMachines[indexPath.row]
        cell.configureCell(machine: machine)
        
        return cell
    }
}

extension ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as! HeaderView
        header.machinesCountLabel.text = "\(self.nearbyMachines.count) vending machines"
        header.detailLabel.text = "Nearest"
        
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Core location methods
extension ViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            refresh()
        case .denied, .restricted:
            let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleName")
            showAlert(title: "Location Error!", message: "Location permission is required to find nearby machines. Go to 'Settings -> \(appName) -> Location' and select 'While Using the App'")
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        
        currentLocation = location
        fetchNearbyMachines()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        print("Location manager failed with error: \(error)")
        showAlert(title: "Location Error!", message: "Cannot determine your location. Please try again.")
        refreshControl.endRefreshing()
    }
}

// MARK: - Cell
class VendingCell : UITableViewCell {
    @IBOutlet weak var machineImageView: UIImageView!
    @IBOutlet weak var machineNameLabel: UILabel!
    @IBOutlet weak var machineDistanceLabel: UILabel!
    @IBOutlet weak var machineAddressLabel: UILabel!
    
    func configureCell(machine: Machine) {
        machineNameLabel.text = machine.name
        machineDistanceLabel.text = machine.distance != nil ? "\(machine.distance!)km" : "<MISSING DISTANCE>"
    }
}

private class HeaderView : UITableViewHeaderFooterView {
    let machinesCountLabel: UILabel = UILabel()
    let detailLabel: UILabel = UILabel()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.backgroundColor = UIColor.white
        
        machinesCountLabel.translatesAutoresizingMaskIntoConstraints = false
        machinesCountLabel.font = UIFont.systemFont(ofSize: 14)
        machinesCountLabel.textColor = UIColor.textGray
        
        detailLabel.translatesAutoresizingMaskIntoConstraints = false
        detailLabel.font = UIFont.systemFont(ofSize: 14)
        detailLabel.textColor = UIColor.squash
        
        self.contentView.addSubview(machinesCountLabel)
        self.contentView.addSubview(detailLabel)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        let viewsDict = ["machinesCountLabel": machinesCountLabel, "detailLabel": detailLabel]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[machinesCountLabel]-[detailLabel]-|", options: [.alignAllCenterY, .alignAllTop, .alignAllBottom], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[machinesCountLabel]-|", options: [], metrics: nil, views: viewsDict))
        
        detailLabel.setContentHuggingPriority(UILayoutPriorityDefaultLow, for: .horizontal)
    }
}
