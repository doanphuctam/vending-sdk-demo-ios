//
//  VendingViewController.swift
//  VendingDemo
//
//  Created by Muhammad Azeem on 9/27/16.
//  Copyright © 2016 Muhammad Azeem. All rights reserved.
//

import UIKit
import VendingSDK

protocol VendingFlow : class {
    func flowComplete()
    func showReceipt(machineName: String, quantity: Int, amount: String, cardMaskedPan: String)
}

enum ButtonState : Int {
    case PairingFailed = 1
}

class VendingViewController : UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var button: UIButton!
    
    weak var delegate: VendingFlow?
    
    var vendController: VendController!
    var machine: Machine!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageControl.numberOfPages = 4
        
        connect()
    }
    
    // MARK: - Private methods
    func connect() {
        self.pageControl.isHidden = false
        self.button.isHidden = true
        
        vendController = VendController(deviceModel: machine.model!, deviceSerial: machine.serial!, serviceId: machine.serviceId!)
        vendController.delegate = self
        do {
            try vendController.connect()
            self.imageView.image = #imageLiteral(resourceName: "Pairing")
            self.pageControl.currentPage = 0
            self.textLabel.text = NSLocalizedString("Pairing with the vending machine...", comment: "")
        } catch {
            print(error)
        }
    }
    
    // MARK: - Action methods
    @IBAction func buttonPressed(_ sender: UIButton) {
        guard let buttonState = ButtonState.init(rawValue: sender.tag) else {
            print("Invalid button state")
            return
        }
        if buttonState == ButtonState.PairingFailed {
            connect()
        }
    }
}

extension VendingViewController : VendControllerDelegate {
    func connected() {
        print("Connected")
        self.imageView.image = #imageLiteral(resourceName: "MakeSelection")
        self.pageControl.currentPage = 1
        self.textLabel.text = NSLocalizedString("Pick the item you want in vending machine by pressing its number", comment: "")
    }
    
    func disconnected(_ error: VendError) {
        print("Connection disconnected: \(error)")
        
        switch error {
        case .connectionTimedOut, .invalidDeviceResponse:
            // TODO: Show different UI
            fallthrough
        default:
            self.imageView.image = #imageLiteral(resourceName: "PairingFailed")
            self.pageControl.isHidden = true
            self.textLabel.text = NSLocalizedString("Pairing failed", comment: "")
            self.button.setTitle(NSLocalizedString("Pair Again", comment: ""), for: .normal)
            self.button.isHidden = false
            self.button.tag = ButtonState.PairingFailed.rawValue
        }
    }
    
    func authRequest(_ amount: NSNumber, token: String?) {
        print("Auth requested")
        
        self.imageView.image = #imageLiteral(resourceName: "Authorizing")
        self.pageControl.currentPage = 2
        self.textLabel.text = NSLocalizedString("Authorizing your payment Info...", comment: "")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.vendController.approveAuth("dummyPayload")
            
            self.imageView.image = #imageLiteral(resourceName: "Approving")
            self.pageControl.currentPage = 3
            self.textLabel.text = NSLocalizedString("Placing the payment...\n$ \(amount)", comment: "")
        }
    }
    
    func processStarted() {
        print("Process started")
    }
    
    func processCompleted(_ finalAmount: NSNumber, processStatus: ProcessStatus, completedPayload: String) {
        print("Process completed")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) { [weak self] in
            // Show receipt
            let amount = String(format: "%0.2f", finalAmount.floatValue)
            self?.delegate?.showReceipt(machineName: self?.machine.name ?? "", quantity: 1, amount: "SGD \(amount)", cardMaskedPan: "**** 4567")
            
            // TODO: Remove it after end session responds properly so this will be handled in onDisconnected
            self?.delegate?.flowComplete()
        }
    }
    
    func timeoutWarning() {
        print("Timeout warning")
        
        self.vendController.keepAlive()
    }
}
